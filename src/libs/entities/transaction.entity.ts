import {
  BaseEntity,
  Column,
  Entity,
  ManyToOne,
  PrimaryGeneratedColumn,
  Unique,
} from 'typeorm';
import { Block } from './block.entity';
@Entity('transactions')
@Unique(['id', 'blockNumber'])
export class Transaction extends BaseEntity {
  @PrimaryGeneratedColumn('uuid')
  id: string;

  @Column()
  from: string;

  @Column({ nullable: true })
  to: string;

  @Column()
  value: string;

  @Column({ name: 'block_number' })
  blockNumber: string;

  @ManyToOne(() => Block, (block) => block.transactions)
  block: Block;
}
