import { BaseEntity, Entity, OneToMany, PrimaryColumn, Unique } from 'typeorm';
import { Transaction } from './transaction.entity';

@Entity('blocks')
@Unique(['number'])
export class Block extends BaseEntity {
  @PrimaryColumn()
  number: string;

  @OneToMany(() => Transaction, (transaction) => transaction.block, {
    onDelete: 'CASCADE',
    cascade: true,
  })
  transactions: Transaction[];
}
