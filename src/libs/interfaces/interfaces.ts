export interface ICreateBlockParams {
  number: string;

  transactions: ITransaction[];
}

export interface ITransaction {
  from: string;
  to: string;
  value: string;
  blockNumber: string;
  id: string;
}

export type TransactionInfo = Omit<ITransaction, 'blockNumber' | 'id'>;
