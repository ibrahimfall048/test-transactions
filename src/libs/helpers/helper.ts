import { ICreateBlockParams, TransactionInfo } from '../interfaces/interfaces';
import * as crypto from 'node:crypto';
import { Block } from '../entities';

export const formatData = (
  number: string,
  transactionsChunk: Record<string, string>[],
): ICreateBlockParams => {
  const transactions = transactionsChunk.map((transaction) => ({
    from: transaction.from,
    to: transaction.to,
    value: transaction.value,
    transactionIndex: transaction.transactionIndex,
    blockNumber: number,
    id: crypto.randomUUID(),
  }));

  return {
    number,
    transactions,
  };
};

export const getFollowingBlockNumber = (blockNumber: string): string => {
  let number: number = +blockNumber;
  number++;
  return '0x'.concat(number.toString(16));
};

export const extractTransactions = (blocks: Block[]): TransactionInfo[] => {
  const transactions: TransactionInfo[] = [];

  blocks.forEach((block) => {
    block.transactions.forEach((transaction) => {
      transactions.push({
        from: transaction.from,
        to: transaction.to,
        value: transaction.value,
      });
    });
  });

  return transactions;
};

export const calculateBalanceChangeForEachAddress = (
  transactions: TransactionInfo[],
): Record<string, number> => {
  const balanceChanges: Record<string, number> = {};
  transactions.forEach((transaction) => {
    const { from, to, value } = transaction;

    if (from) {
      balanceChanges[from] =
        (balanceChanges[from] || 0) - convertValueToNumber(value);
    }

    if (to) {
      balanceChanges[to] =
        (balanceChanges[to] || 0) + convertValueToNumber(value);
    }
  });
  return balanceChanges;
};

export const getAddressWithHighestBalance = (
  balanceChanges: Record<string, number>,
): string | null => {
  let highestBalanceChange = 0;
  let addressWithHighestChange: string | null = null;

  Object.keys(balanceChanges).forEach((address) => {
    const balanceChange = Math.abs(balanceChanges[address]);
    if (balanceChange > highestBalanceChange) {
      highestBalanceChange = balanceChange;
      addressWithHighestChange = address;
    }
  });

  console.log(addressWithHighestChange);
  console.log(balanceChanges[addressWithHighestChange]);

  return addressWithHighestChange;
};

export const convertValueToNumber = (value: string): number => {
  return +value;
};
