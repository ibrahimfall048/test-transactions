import { MigrationInterface, QueryRunner } from 'typeorm';

export class ReplaceTransactionIndexWithId1688798914625
  implements MigrationInterface
{
  name = 'ReplaceTransactionIndexWithId1688798914625';

  public async up(queryRunner: QueryRunner): Promise<void> {
    await queryRunner.query(
      `ALTER TABLE "transactions" DROP CONSTRAINT "FK_a629587e749dda5721fed9a5c39"`,
    );
    await queryRunner.query(
      `ALTER TABLE "transactions" DROP CONSTRAINT "UQ_b06bad2f76a4c33e6b86b77bfe9"`,
    );
    await queryRunner.query(
      `ALTER TABLE "transactions" RENAME COLUMN "transaction_index" TO "id"`,
    );
    await queryRunner.query(
      `ALTER TABLE "transactions" RENAME CONSTRAINT "PK_2b1d3441dd8c3d863f20a4601cf" TO "PK_a219afd8dd77ed80f5a862f1db9"`,
    );
    await queryRunner.query(
      `ALTER TABLE "transactions" DROP CONSTRAINT "PK_a219afd8dd77ed80f5a862f1db9"`,
    );
    await queryRunner.query(`ALTER TABLE "transactions" DROP COLUMN "id"`);
    await queryRunner.query(
      `ALTER TABLE "transactions" ADD "id" uuid NOT NULL DEFAULT uuid_generate_v4()`,
    );
    await queryRunner.query(
      `ALTER TABLE "transactions" ADD CONSTRAINT "PK_a219afd8dd77ed80f5a862f1db9" PRIMARY KEY ("id")`,
    );
    await queryRunner.query(
      `ALTER TABLE "transactions" ADD CONSTRAINT "UQ_26af5dc8a69d36f2fa8f3a2d989" UNIQUE ("id", "block_number")`,
    );
  }

  public async down(queryRunner: QueryRunner): Promise<void> {
    await queryRunner.query(
      `ALTER TABLE "transactions" DROP CONSTRAINT "FK_a629587e749dda5721fed9a5c39"`,
    );
    await queryRunner.query(
      `ALTER TABLE "transactions" DROP CONSTRAINT "UQ_26af5dc8a69d36f2fa8f3a2d989"`,
    );
    await queryRunner.query(
      `ALTER TABLE "transactions" DROP CONSTRAINT "PK_a219afd8dd77ed80f5a862f1db9"`,
    );
    await queryRunner.query(`ALTER TABLE "transactions" DROP COLUMN "id"`);
    await queryRunner.query(
      `ALTER TABLE "transactions" ADD "id" character varying NOT NULL`,
    );
    await queryRunner.query(
      `ALTER TABLE "transactions" ADD CONSTRAINT "PK_a219afd8dd77ed80f5a862f1db9" PRIMARY KEY ("id")`,
    );
    await queryRunner.query(
      `ALTER TABLE "transactions" RENAME CONSTRAINT "PK_a219afd8dd77ed80f5a862f1db9" TO "PK_2b1d3441dd8c3d863f20a4601cf"`,
    );
    await queryRunner.query(
      `ALTER TABLE "transactions" RENAME COLUMN "id" TO "transaction_index"`,
    );
    await queryRunner.query(
      `ALTER TABLE "transactions" ADD CONSTRAINT "UQ_b06bad2f76a4c33e6b86b77bfe9" UNIQUE ("transaction_index", "block_number")`,
    );
  }
}
