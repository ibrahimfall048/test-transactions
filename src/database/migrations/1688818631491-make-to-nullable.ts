import { MigrationInterface, QueryRunner } from 'typeorm';

export class MakeToNullable1688818631491 implements MigrationInterface {
  name = 'MakeToNullable1688818631491';

  public async up(queryRunner: QueryRunner): Promise<void> {
    await queryRunner.query(
      `ALTER TABLE "transactions" ALTER COLUMN "to" DROP NOT NULL`,
    );
  }

  public async down(queryRunner: QueryRunner): Promise<void> {
    await queryRunner.query(
      `ALTER TABLE "transactions" ALTER COLUMN "to" SET NOT NULL`,
    );
  }
}
