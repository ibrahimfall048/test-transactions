import { MigrationInterface, QueryRunner } from 'typeorm';

export class Init1688718298129 implements MigrationInterface {
  name = 'Init1688718298129';

  public async up(queryRunner: QueryRunner): Promise<void> {
    await queryRunner.query(
      `CREATE TABLE "transactions" ("transaction_index" character varying NOT NULL, "from" character varying NOT NULL, "to" character varying NOT NULL, "value" character varying NOT NULL, "block_number" character varying NOT NULL, "blockNumber" character varying, CONSTRAINT "UQ_b06bad2f76a4c33e6b86b77bfe9" UNIQUE ("transaction_index", "block_number"), CONSTRAINT "PK_2b1d3441dd8c3d863f20a4601cf" PRIMARY KEY ("transaction_index"))`,
    );
    await queryRunner.query(
      `ALTER TABLE "blocks" ADD CONSTRAINT "UQ_5c0b8f5cedabb33e58a625f8a7e" UNIQUE ("number")`,
    );
    await queryRunner.query(
      `ALTER TABLE "transactions" ADD CONSTRAINT "FK_a629587e749dda5721fed9a5c39" FOREIGN KEY ("blockNumber") REFERENCES "blocks"("number") ON DELETE NO ACTION ON UPDATE NO ACTION`,
    );
  }

  public async down(queryRunner: QueryRunner): Promise<void> {
    await queryRunner.query(
      `ALTER TABLE "transactions" DROP CONSTRAINT "FK_a629587e749dda5721fed9a5c39"`,
    );
    await queryRunner.query(
      `ALTER TABLE "blocks" DROP CONSTRAINT "UQ_5c0b8f5cedabb33e58a625f8a7e"`,
    );
    await queryRunner.query(`DROP TABLE "transactions"`);
  }
}
