import { Module } from '@nestjs/common';
import { TypeOrmModule } from '@nestjs/typeorm';
import { dbConfig } from '../libs/config/db.config';

@Module({
  imports: [TypeOrmModule.forRootAsync(dbConfig())],
})
export class DatabaseModule {}
