import { Module } from '@nestjs/common';
import { ConfigModule } from '@nestjs/config';
import { DatabaseModule } from './database/database.module';
import { configSchema } from './libs/config/schema.config';
import { BlockModule } from './block/block.module';
import { ScheduleModule } from '@nestjs/schedule';

@Module({
  imports: [
    ConfigModule.forRoot({ isGlobal: true, validationSchema: configSchema }),
    DatabaseModule,
    ScheduleModule.forRoot(),
    BlockModule,
  ],
})
export class AppModule {}
