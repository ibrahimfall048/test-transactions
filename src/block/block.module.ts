import { Module } from '@nestjs/common';
import { BlockService } from './services/block.service';
import { CronJobService } from './services/cron-job.service';
import { TypeOrmModule } from '@nestjs/typeorm';
import { Block } from '../libs/entities';
import { HttpModule } from '@nestjs/axios';
import { BlockController } from './block.controller';

@Module({
  imports: [
    TypeOrmModule.forFeature([Block]),
    HttpModule.register({
      timeout: 5000,
      maxRedirects: 5,
    }),
  ],
  providers: [BlockService, CronJobService],
  controllers: [BlockController],
})
export class BlockModule {}
