export interface IBlockService {
  fetchAndSaveTransactions(): Promise<{ isSuccessful: boolean } | null>;

  findAddressWithHighestBalanceChange(): Promise<{ address: string | null }>;
}
