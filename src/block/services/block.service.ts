import { Injectable, Logger } from '@nestjs/common';
import { IBlockService } from '../block';
import { Block, Transaction } from '../../libs/entities';
import {
  ICreateBlockParams,
  TransactionInfo,
} from '../../libs/interfaces/interfaces';
import { InjectRepository } from '@nestjs/typeorm';
import { Repository } from 'typeorm';
import { HttpService } from '@nestjs/axios';
import { ConfigService } from '@nestjs/config';
import { lastValueFrom, map } from 'rxjs';
import {
  calculateBalanceChangeForEachAddress,
  extractTransactions,
  formatData,
  getAddressWithHighestBalance,
  getFollowingBlockNumber,
} from '../../libs/helpers/helper';

@Injectable()
export class BlockService implements IBlockService {
  private readonly logger = new Logger(BlockService.name);
  private readonly apiBaseUrl: string;
  private readonly apiKey: string;
  private readonly startingBlockNumber: string;
  constructor(
    @InjectRepository(Block)
    private readonly blockRepository: Repository<Block>,
    private readonly configService: ConfigService,
    private readonly httpService: HttpService,
  ) {
    this.apiBaseUrl = configService.get('API_BASE_URL');
    this.apiKey = configService.get('API_KEY');
    this.startingBlockNumber = configService.get('STARTING_BLOCK_NUMBER');
  }

  private async createBlockAndTransactions({
    number,
    transactions,
  }: ICreateBlockParams): Promise<void> {
    try {
      const newBlock = await this.blockRepository.create({
        number,
      });

      newBlock.transactions = transactions as Transaction[];

      await this.blockRepository.save(newBlock);
    } catch (e) {
      this.logger.error('failed to create block and transactions', e);
    }
  }

  private async getLastBlock(): Promise<Block | null> {
    return this.blockRepository.findOne({
      where: {},
      order: {
        number: 'desc',
      },
    });
  }

  private async fetchTransactions(
    blockNumber: string,
  ): Promise<ICreateBlockParams | null> {
    const queryParams = `module=proxy&action=eth_getBlockByNumber&boolean=true&apikey=${this.apiKey}&tag=${blockNumber}`;

    const { result } = await lastValueFrom(
      this.httpService
        .get(`${this.apiBaseUrl}?${queryParams}`)
        .pipe(map((response) => response.data)),
    );

    if (!result) {
      this.logger.error(
        `Failed to fetch transactions from block: ${blockNumber}`,
      );
      return;
    }

    return formatData(blockNumber, result?.transactions);
  }

  async fetchAndSaveTransactions(): Promise<{ isSuccessful: boolean } | null> {
    const block = await this.getLastBlock();

    if (!block?.number) {
      return this.fetchAndSaveTransactionsWithStartingBlockNumber();
    }

    return this.fetchAndSaveTransactionsWithLastBlockNumber(block?.number);
  }

  private async fetchAndSaveTransactionsWithStartingBlockNumber(): Promise<{
    isSuccessful: boolean;
  } | null> {
    try {
      this.logger.debug(`Fetched transactions with starting block number`);

      const { number, transactions } = await this.fetchTransactions(
        this.startingBlockNumber,
      );

      await this.createBlockAndTransactions({ number, transactions });

      this.logger.debug(
        `Initial transactions from block: ${this.startingBlockNumber} fetched and saved successfully`,
      );

      return { isSuccessful: true };
    } catch (e) {
      this.logger.error(
        'Failed to fetch and save transactions from initial block',
      );
    }
  }

  private async fetchAndSaveTransactionsWithLastBlockNumber(
    blockNumber: string,
  ): Promise<{
    isSuccessful: boolean;
  } | null> {
    try {
      this.logger.debug(`Fetched transactions with following block number`);

      const followingBlockNumber: string = getFollowingBlockNumber(blockNumber);

      const { number, transactions } = await this.fetchTransactions(
        followingBlockNumber,
      );

      await this.createBlockAndTransactions({ number, transactions });

      this.logger.debug(
        `Transactions from block: ${followingBlockNumber} fetched and saved successfully`,
      );
      return { isSuccessful: true };
    } catch (e) {
      this.logger.error(
        `Failed to fetch and save transactions from block: ${blockNumber}`,
      );
    }
  }

  async findAddressWithHighestBalanceChange(): Promise<{
    address: string | null;
  }> {
    const transactions = await this.getTransactions();

    const address = getAddressWithHighestBalance(
      calculateBalanceChangeForEachAddress(transactions),
    );

    return {
      address,
    };
  }

  private async getTransactions(): Promise<TransactionInfo[]> {
    const blocks = await this.blockRepository.find({
      order: {
        number: 'desc',
      },
      relations: ['transactions'],
      take: 100,
      select: ['transactions'],
    });

    return extractTransactions(blocks);
  }
}
