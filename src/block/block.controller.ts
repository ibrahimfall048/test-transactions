import { Controller, Get, HttpCode, HttpStatus } from '@nestjs/common';
import { BlockService } from './services/block.service';

@Controller('blocks')
export class BlockController {
  constructor(private readonly blockService: BlockService) {}

  @HttpCode(HttpStatus.OK)
  @Get('highest-balance-change')
  findAddressWithHighestBalanceChange(): Promise<{ address: string | null }> {
    return this.blockService.findAddressWithHighestBalanceChange();
  }
}
