<h1 align="center"> TEST TRANSACTIONS  </h1> 

## Table of Contents

- [Installation](#Installation)
- [Running the app](#Running-the-app)
- [Migrations](#Migrations)
- [API Docs](#api-docs)



## Installation

```bash
# clone project
$ git clone git@gitlab.com:ibrahimfall048/test-transactions.git
```

```bash
# env variable docker-compose 
Create temporary .env file for docker-compose env variables

# run container
$ docker-compose up

# env variables
copy db env variables to .deploy/local/.env 
```

```bash
# env variables
Add environment variables to .deploy/local/.env 
```

```bash
# install dependencies
$ yarn 
```

## Running the app

```bash
# development
$ yarn start

# watch mode
$ yarn  start:dev
```

## Migrations

```bash
# run migrations 
$ yarn migrations:run
```

## API Docs

## highest-balance-change-address
### Request 

`GET 'api/blocks/highest-balance-change'`

### Response
```bash
HTTP/1.1 200 OK
Date: Sun, 09 Jul 2023 08:20:30 GMT
Status: 200 OK
Connection: close
Content-Type: application/json
Content-Length: 2
{ "address": "xxxxxxxxxxxxxxxxxxxxxxxxxxxxxxx" }
```

